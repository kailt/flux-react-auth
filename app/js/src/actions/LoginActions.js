import alt from '../libs/alt';
import AuthUtils from '../utils/AuthUtils';

// http://stackoverflow.com/questions/26632415/where-should-ajax-request-be-made-in-flux-app
class LoginActions {
  loginUser(username, password) {
    let promise = AuthUtils.login(username, password);

    this.dispatch({ username, password }); // dispatch current action

    // Access other actions within an action with this.actions
    // https://github.com/goatslacker/alt/issues/193
    promise.then(
      (body) => { this.actions.loginUserSuccess(username, body.token); },
      (err) => { this.actions.loginUserError(err); }
    );
  }

  logoutUser() {
    this.dispatch();
  }

  loginUserSuccess(username, token) {
    this.dispatch({ username, token });
  }

  loginUserError(err) {
    this.dispatch(err);
  }
}

export default alt.createActions(LoginActions);
