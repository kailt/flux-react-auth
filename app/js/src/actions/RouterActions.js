import alt from '../libs/alt';

class RouterActions {
  storeNextTransitionPath(path) {
    this.dispatch(path);
  }
}

export default alt.createActions(RouterActions);
