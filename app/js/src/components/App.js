import React from 'react';
import { Grid } from 'react-bootstrap';
import { RouteHandler } from 'react-router';

import LoginStore from '../stores/LoginStore';
import RouterStore from '../stores/RouterStore';

import router from '../libs/router';

export default class App extends React.Component {
  // alt
  constructor(props) {
    super(props);

    this.state = LoginStore.getState();
    this.onLoginChange = this.onLoginChange.bind(this);
  }

  componentDidMount() {
    LoginStore.listen(this.onLoginChange);
  }

  componentWillUnmount() {
    LoginStore.unlisten(this.onLoginChange);
  }

  render() {
    console.log('render App');
    return (
      <Grid fluid={true}>
        <RouteHandler {...this.props} {...this.state} />
      </Grid>
    );
  }

  // LoginSrore onChange handler
  onLoginChange() {
    this.setState(LoginStore.getState());

    // default transition path is /
    const transitionPath = RouterStore.getState().nextPath || '/';

    console.log('App onLoginChange. isLoggedIn:', !!this.state.token, 'transitionPath:', transitionPath);

    if (!!this.state.token) {
      router.transitionTo(transitionPath);
    } else {
      router.transitionTo('/login');
    }
  }
}
