import React from 'react';

import authenticated from '../decorators/authenticated';

@authenticated
export default class Home extends React.Component {
  render() {
    return (
      <h1>Home</h1>
    );
  }
}
