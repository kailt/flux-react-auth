import React from 'react';
import { Grid, Row, Col, Panel, Input, ButtonInput } from 'react-bootstrap';
import _ from 'lodash';

import LoginActions from '../actions/LoginActions';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };

    this.updateState = this.updateState.bind(this);
    this.loginUser = this.loginUser.bind(this);
  }

  render() {
    console.log('render Login');

    const loginHeader = <h3>Login</h3>;
    const loginUser = this.loginUser;
    const updateState = this.updateState;

    return (
      <Row>
        <Col md={4} mdOffset={4}>
          <Panel className="login-panel" header={loginHeader}>
            <form role="form" onSubmit={loginUser}>
              <fieldset>
                <Input placeholder="Username" name="username" type="text" autoFocus={true} onChange={updateState} />
                <Input placeholder="Password" name="password" type="password" defaultValue="" onChange={updateState} />
                <ButtonInput type="submit" value="Login" bsStyle='success' bsSize='large' block />
              </fieldset>
            </form>
          </Panel>
        </Col>
      </Row>
    );
  }

  loginUser(e) {
    e.preventDefault();
    const { username, password } = this.state;
    LoginActions.loginUser(username, password);

    console.log('submit login. username: ', username, 'password:', password);
  }

  updateState(e) {
    const { name, value } = e.target;

    // http://stackoverflow.com/questions/11508463/javascript-set-object-key-by-variable
    let obj = {};
    obj[name] = value;

    this.setState(_.assign({}, this.state, obj));
  }
}
