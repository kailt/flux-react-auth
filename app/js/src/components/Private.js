import React from 'react';

import authenticated from '../decorators/authenticated';

@authenticated
export default class Private extends React.Component {
  render() {
    return (
      <h1>Private</h1>
    );
  }
}
