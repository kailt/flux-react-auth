import React from 'react';
import LoginStore from '../stores/LoginStore';
import RouterActions from '../actions/RouterActions';

// https://medium.com/@dan_abramov/mixins-are-dead-long-live-higher-order-components-94a0d2f9e750
export default (Component) => {
  return class AuthenticatedComponent extends React.Component {
    constructor(props) {
      super(props);
      this.state = LoginStore.getState();

      this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
      LoginStore.listen(this.onChange);
    }

    componentWillUnmount() {
      LoginStore.unlisten(this.onChange);
    }

    // react-router
    static willTransitionTo(transition) {
      const loginState =  LoginStore.getState();

      console.log('Authenticated component. Next transition path:', transition.path, 'logged in:', !!loginState.token);

      if (!loginState.token) {

        let transitionPath = transition.path;

        //store next path in RouterStore for redirecting after authentication
        //as opposed to storing in the router itself with:
        // transition.redirect('/login', {}, {'nextPath' : transition.path});
        RouterActions.storeNextTransitionPath(transitionPath);

        //go to login page
        transition.redirect('/login');
      }
    }

    render() {
      return <Component {...this.props} {...this.state} />;
    }

    onChange() {
      this.setState(LoginStore.getState());
    }
  };
};
