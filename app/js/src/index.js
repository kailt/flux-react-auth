import React from 'react';

import router from './libs/router';

router.run(function(Handler) {
  React.render(<Handler />, document.getElementById('app'));
});
