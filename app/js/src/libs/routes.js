import React from 'react';
import { Route, DefaultRoute } from 'react-router';

import App from '../components/App';
import Login from '../components/Login';
import Home from '../components/Home';
import Private from '../components/Private';
import Public from '../components/Public';

export default (
  <Route path="/" handler={App}>
    <DefaultRoute handler={Home} />
    <Route name="login" path="/login" handler={Login} />
    <Route name="private" path="/private" handler={Private} />
    <Route name="public" path="/public" handler={Public} />
  </Route>
);
