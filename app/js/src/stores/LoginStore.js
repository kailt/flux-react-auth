import alt from '../libs/alt';
import LoginActions from '../actions/LoginActions';

// http://wmyers.github.io/technical/react-flux/Authentication-routing-in-a-React-Flux-application/
class LoginStore {
  constructor() {
    // connect with actions
    this.bindActions(LoginActions);

    // initial state
    this.username = null;
    this.token = null;

    // attempt auto login on browser refresh
    this.autoLogin();
  }

  loginUser({ username, password }) {
    console.log('login request sent. username: ', username, 'password: ', password);
  }

  loginUserSuccess({ username, token }) {
    localStorage.setItem('api_auth', JSON.stringify({ username, token }));

    this.setState({
      username: username,
      token: token
    });

    console.log('login success. username:', username, 'token:', token);
  }

  loginUserError(err) {
    console.log(err);
  }

  logoutUser() {
    localStorage.setItem('api_auth', null);

    this.setState({
      username: null,
      token: null
    });

    console.log('logout success');
  }

  autoLogin() {
    const apiAuth = JSON.parse(localStorage.getItem('api_auth'));

    if (apiAuth) {
      const { username, token } = apiAuth;

      // can't use this.setState here because in the constructor
      // we still don't have an instance
      this.username = username;
      this.token = token;

      console.log('autoLogin ok. user:', username, 'token:', token);
    } else {
      console.log('autoLogin fail');
    }
  }
}

export default alt.createStore(LoginStore, 'LoginStore');
