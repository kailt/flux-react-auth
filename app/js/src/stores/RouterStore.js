import alt from '../libs/alt';
import RouterActions from '../actions/RouterActions';

class RouterStore {
  constructor() {
    this.bindActions(RouterActions);

    this.nextPath = null;
  }
  storeNextTransitionPath(path) {
    this.nextPath = path;
  }
}

export default alt.createStore(RouterStore, 'RouterStore');
