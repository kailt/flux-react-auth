var gulp        = require('gulp');
var gutil       = require('gulp-util');
var source      = require('vinyl-source-stream');
var babelify    = require('babelify');
var watchify    = require('watchify');
var exorcist    = require('exorcist');
var browserify  = require('browserify');
var browserSync = require('browser-sync').create();
var buffer      = require('vinyl-buffer');
var uglify      = require('gulp-uglify');
var glob        = require('glob');
var uglifyify   = require('uglifyify');
var envify      = require('envify/custom');

process.env.NODE_ENV = 'development'; // For React minification

function browserifyWatch(watch) {
  var b = browserify({
    debug: true // Enable sourcemaps
  })
    .transform(babelify.configure({ // es6 transform
        sourceMapRelative: './app/js/src',
        stage: 1, // es7 - https://babeljs.io/docs/usage/experimental/
        // optional: ["es7.classProperties"],

        // Size reduction - trial and error
        // externalHelpers: true, // https://github.com/babel/babel/issues/1262
        // optional: ["runtime"], // https://www.reddit.com/r/javascript/comments/3dcp5h/react_babel_bloat/ct3xmaus

        // loose: "all" // http://babeljs.io/docs/advanced/loose/
    }));

  if (process.env.NODE_ENV === 'production') {
    b.transform({global: true}, uglifyify); // global uglify before browserify
  }

  if(watch) {
    // if watch is enable, wrap this bundle inside watchify
    b = watchify(b);
    b.on('update', function() {
      bundle(b);
    });
  }
  bundle(b);
}

function bundle(b) {
  gutil.log('Compiling js bundle...');
  // http://stackoverflow.com/questions/28372845/use-glob-matching-when-passing-files-to-browserify-in-gulp
  var files = glob.sync('./app/js/src/**/*.js');
  // gutil.log(files);
  files.map(function(file) {
    b.add(file);
  });
  var bstream = b.bundle()
    .on('error', function (err) {
        gutil.log(err.message);
        browserSync.notify("Browserify Error!");
        this.emit("end");
    })
    .pipe(exorcist('./app/js/dist/bundle.js.map')) // sourcemap
    .pipe(source('bundle.js'))
    .pipe(buffer());

  if (process.env.NODE_ENV === 'production') {
    bstream = bstream.pipe(uglify());
  }
  return bstream.pipe(gulp.dest('./app/js/dist'))
    .pipe(browserSync.stream({once: true}))
    .on('finish', function () {
      gutil.log('JS built!');
    });
}

// browserify
gulp.task('browserify', function() {
  browserifyWatch(false);
});

gulp.task('browserify-watch', function() {
  browserifyWatch(true);
});

/**
 * CSS task
 */
gulp.task('css', function() {
  gutil.log('Injecting CSS...');
  return gulp.src("./app/css/*.css")
    .pipe(browserSync.stream());
});

/**
 * Global watch task
 */
gulp.task('watch', ['browserify-watch'], function() {
  // Watch html changes and reload
  // http://stackoverflow.com/questions/21689334/gulp-globbing-how-to-watch-everything-below-directory
  gulp.watch("./app/**/*.html").on("change", browserSync.reload);
  gulp.watch("./app/css/*.css", ['css']);
});

/**
 * Build task
 */
gulp.task('build', ['browserify', 'css']);

/**
 * First bundle, then serve from the ./app directory
 */
gulp.task('default', ['build', 'watch'], function() {
    browserSync.init({
        server: "./app", // static site
        index: "index.html"
        // proxy: "localhost:3000"
    });
});
